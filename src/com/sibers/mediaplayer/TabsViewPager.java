package com.sibers.mediaplayer;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class TabsViewPager extends SherlockFragmentActivity {

	ViewPager mViewPager;
	TabsAdapter mTabsAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		InitTabs();
	}
	
	void InitTabs() {
		ActionBar bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.pager);
		mViewPager.setOffscreenPageLimit(1);
		
		setContentView(mViewPager);

		mTabsAdapter = new TabsAdapter(this, mViewPager);

		mTabsAdapter.addTab(bar.newTab().setText("Playlist"),
				PlaylistFragment.class, null);
		mTabsAdapter.addTab(bar.newTab().setText("Player"),
				PlayerFragment.class, null);
//		mTabsAdapter.addTab(bar.newTab().setText("Privacy").setIcon(R.drawable.privacy),
//				PrivacyFragment.class, null);
//		mTabsAdapter.addTab(bar.newTab().setText("Settings").setIcon(R.drawable.sett),
//				SettingsFragment.class, null);
//		mTabsAdapter.addTab(bar.newTab().setText("Updates").setIcon(R.drawable.updates),
//				UpdatesFragment.class, null);
//		mTabsAdapter.addTab(bar.newTab().setText("Profile").setIcon(R.drawable.info),
//				ProfileFragment.class, null);
	}
	
	public Fragment getActiveFragment(ViewPager container, int position) {
		   String name = makeFragmentName(container.getId(), position);
		   return (Fragment) getSupportFragmentManager().findFragmentByTag(name);
		  }

	private String makeFragmentName(int viewId, int index) {
		return "android:switcher:" + viewId + ":" + index;
	}
	
	public static class TabsAdapter extends FragmentPagerAdapter implements ActionBar.TabListener, ViewPager.OnPageChangeListener {
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;
		private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mActionBar = activity.getSupportActionBar();
			mViewPager = pager;
			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
			TabInfo info = new TabInfo(clss, args);
			tab.setTag(info);
			tab.setTabListener(this);
			mTabs.add(info);
			mActionBar.addTab(tab);
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);
			return Fragment.instantiate(mContext, info.clss.getName(), info.args);
		}

		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		}

		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);
		}

		public void onPageScrollStateChanged(int state) {
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Object tag = tab.getTag();
			for (int i = 0; i < mTabs.size(); i++) {
				if (mTabs.get(i) == tag) {
					mViewPager.setCurrentItem(i);
				}
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		getActiveFragment(mViewPager,0).onActivityResult(requestCode, resultCode, data);
	}
}