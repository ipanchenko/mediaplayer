package com.sibers.mediaplayer;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

public class Tracklist {
	private static Tracklist instance = null;
	private ArrayList<Song> tracks = new ArrayList<Song>();
	private Song mCurrent;
	private ArrayList<TrackCallback> mCallbacks = new ArrayList<TrackCallback>();
	public Song getCurrent() {
		return mCurrent;
	}

	public int getCurrent(Song song) {
		return tracks.indexOf(song);
	}
	
	public void setCurrent(int current) {
		mCurrent = tracks.get(current);
		sendCallbacks();
	}
	
	public static Tracklist getInstance() {
		if(instance == null) {
			instance = new Tracklist();
		}
		return instance;
	}
	
	public ArrayList<Song> getTracks() {
		return tracks;
	}

	public void setTracks(ArrayList<Song> tracks) {
		this.tracks = tracks;
	}

	public void addTrack(Song song) {
		tracks.add(song);
	}
	
	public void addTracks(ArrayList<Song> songs) {
		tracks.addAll(songs);
		if(mCurrent == null) {
			setCurrent(0);
		}
	}
	
	public Song getNext() {
		if(tracks.indexOf(mCurrent)+1 >= tracks.size())
			return null;
		else {
			mCurrent = tracks.get(tracks.indexOf(mCurrent)+1); 
			sendCallbacks();
			return mCurrent;
		}
	}
	
	public Song getPrev() {
		if(tracks.indexOf(mCurrent)-1 < 0) {
			return null;
		} else {
			mCurrent = tracks.get(tracks.indexOf(mCurrent)-1);
			sendCallbacks();
			return mCurrent;
		}
	}
	
	public void fillTracklist(Context context) {
		addTracks(getAudioFiles(context));
	}
	
	private ArrayList<Song> getAudioFiles(Context context) {
		ArrayList<Song> result = new ArrayList<Song>();
		final Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		final String where = MediaStore.Audio.Media.IS_MUSIC + "=1";
		final Cursor cursor = context.getContentResolver().query(uri, Song.FILLED_PROJECTION, where, null, null);
		while(cursor.moveToNext()) {
			Song song = new Song();
			song.populate(cursor);
			result.add(song);
		}
		return result;
	}
	
	public void addCallback(TrackCallback callback) {
		mCallbacks.add(callback);
	}
	
	public void removeCallback(TrackCallback callback) {
		mCallbacks.remove(callback);
	}
	
	private void sendCallbacks() {
		for (int i = 0; i < mCallbacks.size(); i++) {
			mCallbacks.get(i).call();
		}
	}
	public interface TrackCallback {
		public abstract void call();
	}
}
