package com.sibers.mediaplayer;

import com.sibers.mediaplayer.PlayerService.State;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class HomeWidget extends AppWidgetProvider {
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		updateWidget(context, appWidgetManager, Tracklist.getInstance().getCurrent(), State.PAUSE);
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
	}
	
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
	}
	
	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
	}
	
	public static void updateWidget(Context context, AppWidgetManager manager, Song song, State state) {

		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.home_widget);

		Intent intent;
		PendingIntent pendingIntent;

		ComponentName service = new ComponentName(context, PlayerService.class);

		if(song != null) {
			views.setTextViewText(R.id.artist, song.artist);
			views.setTextViewText(R.id.title, song.title);
			views.setTextViewText(R.id.albom, song.album);
		}
		
		int playButton = state == State.PLAY ? R.drawable.pause : R.drawable.play;
		views.setImageViewResource(R.id.play, playButton);
		
		intent = new Intent(PlayerService.ACTION_PREVIOUS_SONG).setComponent(service);
		pendingIntent = PendingIntent.getService(context, 0, intent, 0);
		views.setOnClickPendingIntent(R.id.prev, pendingIntent);

		intent = new Intent(PlayerService.ACTION_PLAY_PAUSE).setComponent(service);
		pendingIntent = PendingIntent.getService(context, 0, intent, 0);
		views.setOnClickPendingIntent(R.id.play, pendingIntent);

		intent = new Intent(PlayerService.ACTION_NEXT_SONG).setComponent(service);
		pendingIntent = PendingIntent.getService(context, 0, intent, 0);
		views.setOnClickPendingIntent(R.id.next, pendingIntent);

		manager.updateAppWidget(new ComponentName(context, HomeWidget.class), views);
	}
}
