package com.sibers.mediaplayer;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

public class Song implements Parcelable {
	
	public static final String[] FILLED_PROJECTION = {
		MediaStore.Audio.Media._ID,
		MediaStore.Audio.Media.DATA,
		MediaStore.Audio.Media.TITLE,
		MediaStore.Audio.Media.ALBUM,
		MediaStore.Audio.Media.ARTIST,
		MediaStore.Audio.Media.ALBUM_ID,
		MediaStore.Audio.Media.ARTIST_ID,
		MediaStore.Audio.Media.DURATION,
		MediaStore.Audio.Media.TRACK,
	};
	
	public long id;
	public long albumId;
	public long artistId;
	public String path;
	public String title;
	public String album;
	public String artist;
	public long duration;
	public int trackNumber;
	
	public void populate(Cursor cursor) {
		id = cursor.getLong(0);
		path = cursor.getString(1);
		title = cursor.getString(2);
		album = cursor.getString(3);
		artist = cursor.getString(4);
		albumId = cursor.getLong(5);
		artistId = cursor.getLong(6);
		duration = cursor.getLong(7);
		trackNumber = cursor.getInt(8);
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(path);
		dest.writeString(title);
		dest.writeString(album);
		dest.writeString(artist);
		dest.writeLong(albumId);
		dest.writeLong(artistId);
		dest.writeLong(duration);
		dest.writeInt(trackNumber);
	}
	
	public Song(Parcel in) {
		id = in.readLong();
		path = in.readString();
		title = in.readString();
		album = in.readString();
		artist = in.readString();
		albumId = in.readLong();
		artistId = in.readLong();
		duration = in.readLong();
		trackNumber = in.readInt();
    }

	public Song() {
		// TODO Auto-generated constructor stub
	}

	public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }
 
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };
    
    public int hashCode() {
    	return path.hashCode();
    };
}
