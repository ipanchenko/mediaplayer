package com.sibers.mediaplayer;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SongsListAdapter extends ArrayAdapter<Song> {
	Context context;
	public SongsListAdapter(Context context, int textViewResourceId, ArrayList<Song> objects) {
		super(context, 0, objects);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(android.R.layout.simple_list_item_2, null);
		}
		Song item = (Song) getItem(position);
		TextView tv1 = (TextView)convertView.findViewById(android.R.id.text1);
		TextView tv2 = (TextView)convertView.findViewById(android.R.id.text2);
		tv1.setText(item.trackNumber+" "+item.title);
		tv2.setText(item.artist+" "+item.album);
		return convertView;
	}
}
