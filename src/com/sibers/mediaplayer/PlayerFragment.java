package com.sibers.mediaplayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.sibers.mediaplayer.Tracklist.TrackCallback;

public class PlayerFragment extends SherlockFragment {
	
	TextView mArtist;
	TextView mTitle;
	TextView mAlbom;
	PlayerTrackCallback mCallback;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = (View)inflater.inflate(R.layout.player_fragment, null);
		
		mArtist = (TextView)view.findViewById(R.id.artist);
		mTitle = (TextView)view.findViewById(R.id.title);
		mAlbom = (TextView)view.findViewById(R.id.albom);
		mArtist.setText(Tracklist.getInstance().getCurrent().artist);
		mTitle.setText(Tracklist.getInstance().getCurrent().title);
		mAlbom.setText(Tracklist.getInstance().getCurrent().album);
		
		mCallback = new PlayerTrackCallback();
		Tracklist.getInstance().addCallback(mCallback);
		
		ImageButton next = (ImageButton)view.findViewById(R.id.next);
		ImageButton prev = (ImageButton)view.findViewById(R.id.previous);
		ImageButton play_pause = (ImageButton)view.findViewById(R.id.play_pause);
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), PlayerService.class);
				intent.setAction(PlayerService.ACTION_NEXT_SONG);
				getActivity().startService(intent);
			}
		});
		prev.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), PlayerService.class);
				intent.setAction(PlayerService.ACTION_PREVIOUS_SONG);
				getActivity().startService(intent);
			}
		});
		play_pause.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), PlayerService.class);
				intent.setAction(PlayerService.ACTION_PLAY_PAUSE);
				getActivity().startService(intent);
//				long eventtime = SystemClock.uptimeMillis(); 
//				Intent downIntent = new Intent(Intent.ACTION_MEDIA_BUTTON, null);
//				KeyEvent downEvent = new KeyEvent(eventtime, eventtime,
//						KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, 0);
//				downIntent.putExtra(Intent.EXTRA_KEY_EVENT, downEvent);
//				getActivity().sendOrderedBroadcast(downIntent, null); 
			}
		});
		return view;
	}
	
	@Override
	public void onDestroy() {
		Tracklist.getInstance().removeCallback(mCallback);
		super.onDestroy();
	}
	private class PlayerTrackCallback implements TrackCallback {
		@Override
		public void call() {
			mArtist.setText(Tracklist.getInstance().getCurrent().artist);
			mTitle.setText(Tracklist.getInstance().getCurrent().title);
			mAlbom.setText(Tracklist.getInstance().getCurrent().album);
		}
	}
}
