package com.sibers.mediaplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

public class PlayerService extends Service implements
		MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

	public enum State {PLAY, PAUSE, STOPED, NOT_PREPARED}
	private MediaPlayer mMediaPlayer;
	private WakeLock mWakeLock;
	private State mState = State.NOT_PREPARED;
	private Song mCurrent;
	public static final String ACTION_PLAY_PAUSE = "com.sibers.mediaplayer.action.PLAY_PAUSE";
	public static final String ACTION_PAUSE = "com.sibers.mediaplayer.action.ACTION_PAUSE";
	public static final String ACTION_PREVIOUS_SONG = "com.sibers.mediaplayer.action.PREVIOUS_SONG";
	public static final String ACTION_NEXT_SONG = "com.sibers.mediaplayer.action.NEXT_SONG";
	public static final String ACTION_PLAY = "com.sibers.mediaplayer.action.ACTION_PLAY";
	public static final String ACTION_STOP_SELF = "com.sibers.mediaplayer.action.ACTION_STOP_SELF";
	private static final int NOTIFICATION_ID = 1337;
	private Tracklist mTracklist = Tracklist.getInstance();
	private SharedPreferences mPref;
	private ComponentName mRemoteControlReceiver;
	private AudioManager mAudioManager;
	
	@Override
	public void onCreate() {
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mMediaPlayer.setOnCompletionListener(this);
		mMediaPlayer.setOnErrorListener(this);
		
		if(mTracklist.getTracks().size() == 0) {
			mTracklist.fillTracklist(this);
		}
		
		mPref = getSharedPreferences("PlayerService", MODE_PRIVATE);
		int current = mPref.getInt("current", 0);
		mTracklist.setCurrent(current);
		
		mAudioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
		mRemoteControlReceiver = new ComponentName(this, RemoteControlReceiver.class);
		mAudioManager.registerMediaButtonEventReceiver(mRemoteControlReceiver);
		
		super.onCreate();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			String action = intent.getAction();
			if(ACTION_PLAY_PAUSE.equals(action)) {
				setPlayPause();
			}
			if(ACTION_PREVIOUS_SONG.equals(action)) {
				play(mTracklist.getPrev());
			}
			if(ACTION_NEXT_SONG.equals(action)) {
				play(mTracklist.getNext());
			}
			if(ACTION_PLAY.equals(action)) {
				play(mTracklist.getCurrent());
			}
			if(ACTION_STOP_SELF.equals(action)) {
				stopForeground(true);
				stopSelf();
			}
			if(ACTION_PAUSE.equals(action)) {
				setPause();
			}
		}
		return START_STICKY;
	}
	
	private void setPlayPause() {
		if(mState == State.PLAY) {
			mMediaPlayer.pause();
			mState = State.PAUSE;
			WakeLock(false);
		} else if(mState == State.PAUSE) {
			mMediaPlayer.start();
			mState = State.PLAY;
			WakeLock(true);
		} else {
			play(mTracklist.getCurrent());
			mState = State.PLAY;
			WakeLock(true);
		}
		startForeground(NOTIFICATION_ID, buildNotify());
		updateWidgets();
	}

	private void setPause() {
		if(mState == State.PLAY) {
			mMediaPlayer.pause();
			mState = State.PAUSE;
			WakeLock(false);
		}
	}
	@Override
	public boolean onError(MediaPlayer player, int what, int extra) {
		Log.e("MediaPlayer", "MediaPlayer error: " + what + ' ' + extra);
		return true;
	}

	@Override
	public void onCompletion(MediaPlayer player) {
		Song next = mTracklist.getNext();
		if(next == null) {
			
			return;
		}
		else {
			play(next);
		}
	}

	private void play(Song song) {
		if(song == null)
			return;
		try {
			mCurrent = song;
			mMediaPlayer.reset();
			mMediaPlayer.setDataSource(mCurrent.path);
			mMediaPlayer.prepare();
			mMediaPlayer.start();
			mState = State.PLAY;
			startForeground(NOTIFICATION_ID, buildNotify());
			mPref.edit().putInt("current", mTracklist.getCurrent(mCurrent)).commit();
			updateWidgets();
		} catch (Exception e) {
			e.printStackTrace();
			//Handle this
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		stopForeground(true);

		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		
		mAudioManager.unregisterMediaButtonEventReceiver(mRemoteControlReceiver);
		WakeLock(false);
		stopForeground(true);
		super.onDestroy();
	}
	
	private Notification buildNotify() {
		Intent intent = new Intent(this, PlaylistFragment.class);
		RemoteViews views = new RemoteViews(getPackageName(), R.layout.notification);
		views.setTextViewText(R.id.title, mTracklist.getCurrent().title);
		views.setTextViewText(R.id.artist, mTracklist.getCurrent().artist);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext());
		builder.setOngoing(true);
		builder.setSmallIcon(R.drawable.play);
		builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0));
		Notification notif  = builder.build();
		notif.contentView = views;
		Log.e("PlayerService", "SDK_INT = " + Build.VERSION.SDK_INT);
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
			setListners(views);
		}
		return notif;
	}

	private void setListners(RemoteViews views) {
		int playButton = mState == State.PLAY ? R.drawable.pause : R.drawable.play;
		views.setImageViewResource(R.id.play_pause_btn, playButton);
		ComponentName service = new ComponentName(this, PlayerService.class);
		Intent playPause = new Intent(PlayerService.ACTION_PLAY_PAUSE);
		playPause.setComponent(service);
		views.setOnClickPendingIntent(R.id.play_pause_btn, PendingIntent.getService(this, 0, playPause, 0));
		Intent next = new Intent(PlayerService.ACTION_NEXT_SONG);
		next.setComponent(service);
		views.setOnClickPendingIntent(R.id.next, PendingIntent.getService(this, 0, next, 0));
		Intent close = new Intent(PlayerService.ACTION_STOP_SELF);
		close.setComponent(service);
		views.setOnClickPendingIntent(R.id.close, PendingIntent.getService(this, 0, close, 0));

	}
	
	private void WakeLock(boolean locked) {
		if(locked) {
			if (mWakeLock == null || !mWakeLock.isHeld()) {
				PowerManager powerManager = (PowerManager)getSystemService(POWER_SERVICE);
				mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MusicLock");
			}
		} else {
			if (mWakeLock != null && mWakeLock.isHeld())
				mWakeLock.release();
		}
	}
	
	private void updateWidgets() {
		AppWidgetManager manager = AppWidgetManager.getInstance(this);
		HomeWidget.updateWidget(this, manager, mTracklist.getCurrent(), mState);
	}
}
