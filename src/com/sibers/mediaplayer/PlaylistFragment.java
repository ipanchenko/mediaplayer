package com.sibers.mediaplayer;

import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;

public class PlaylistFragment extends SherlockFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.playlist_fragment, null);
		ListView lListView = (ListView)view.findViewById(R.id.list);
		final ArrayList<Song> songs = getAudioFiles();
		Tracklist.getInstance().addTracks(songs);
		getActivity().startService(new Intent(getActivity(), PlayerService.class));
		lListView.setAdapter(new SongsListAdapter(getActivity(), 0, songs));
		lListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,long id) {
				Tracklist.getInstance().setCurrent(position);
				Intent intent = new Intent(getActivity(), PlayerService.class);
				intent.setAction(PlayerService.ACTION_PLAY);
				getActivity().startService(intent);
			}
		});
		return view;
	}


	
	ArrayList<Song> getAudioFiles() {
		ArrayList<Song> result = new ArrayList<Song>();
		final Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		final String where = MediaStore.Audio.Media.IS_MUSIC + "=1";
		final Cursor cursor = getActivity().getContentResolver().query(uri, Song.FILLED_PROJECTION, where, null, null);
		while(cursor.moveToNext()) {
			Song song = new Song();
			song.populate(cursor);
			result.add(song);
		}
		return result;
	}
    
}
