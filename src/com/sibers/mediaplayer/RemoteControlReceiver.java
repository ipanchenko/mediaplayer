package com.sibers.mediaplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

public class RemoteControlReceiver extends BroadcastReceiver {

	private static final int DOUBLE_CLICK_FAILSAFE = 600;
	private static long mPrevTime = 0;
	static int mPrevEvent = KeyEvent.KEYCODE_MEDIA_STOP;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
			Intent service_intent = new Intent(context, PlayerService.class);
			KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
			if(mPrevEvent == event.getKeyCode())
				if(event.getEventTime() - mPrevTime < DOUBLE_CLICK_FAILSAFE )
					return;
			mPrevEvent = event.getKeyCode();
			mPrevTime = event.getEventTime();
//			if (KeyEvent.KEYCODE_MEDIA_CLOSE == event.getKeyCode()) {
//
//			}
//			if (KeyEvent.KEYCODE_MEDIA_EJECT == event.getKeyCode()) {
//
//			}
			if (KeyEvent.KEYCODE_MEDIA_NEXT == event.getKeyCode()) {
				service_intent.setAction(PlayerService.ACTION_NEXT_SONG);
				context.startService(service_intent);
			}
			if (KeyEvent.KEYCODE_MEDIA_PAUSE == event.getKeyCode()) {
				service_intent.setAction(PlayerService.ACTION_PAUSE);
				context.startService(service_intent);
			}
			if (KeyEvent.KEYCODE_MEDIA_PLAY == event.getKeyCode()) {
				service_intent.setAction(PlayerService.ACTION_PLAY);
				context.startService(service_intent);
			}
			if (KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE == event.getKeyCode()) {
				service_intent.setAction(PlayerService.ACTION_PLAY_PAUSE);
				context.startService(service_intent);
			}
//			if (KeyEvent.KEYCODE_MEDIA_RECORD == event.getKeyCode()) {
//
//			}
//			if (KeyEvent.KEYCODE_MEDIA_REWIND == event.getKeyCode()) {
//			
//			}
			if (KeyEvent.KEYCODE_MEDIA_PREVIOUS == event.getKeyCode()) {
				service_intent.setAction(PlayerService.ACTION_PREVIOUS_SONG);
				context.startService(service_intent);
			}
			if (KeyEvent.KEYCODE_MEDIA_STOP == event.getKeyCode()) {
				service_intent.setAction(PlayerService.ACTION_STOP_SELF);
				context.startService(service_intent);
			}

		}
	}

}
